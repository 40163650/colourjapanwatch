#include "pebble.h"
#include "gbitmap_color_palette_manipulator.h"
  
static Window *window;
/*

 There's only enough memory to load about 6 of 10 required images
 so we have to swap them in & out...

 We have one "slot" per digit location on screen.

 Because layers can only have one parent we load a digit for each
 slot--even if the digit image is already in another slot.

 Slot on-screen layout:
     0 1
     2 3
     
*/
#define TOTAL_IMAGE_SLOTS 6

#define NUMBER_OF_IMAGES 11

// These images are 72 x 84 pixels (i.e. a quarter of the display),
// black and white with the digit character centered in the image.
// (As generated by the `fonttools/font2png.py` script.)
const int IMAGE_RESOURCE_IDS[NUMBER_OF_IMAGES] = {
  RESOURCE_ID_IMAGE_0, RESOURCE_ID_IMAGE_1, RESOURCE_ID_IMAGE_2,
  RESOURCE_ID_IMAGE_3, RESOURCE_ID_IMAGE_4, RESOURCE_ID_IMAGE_5,
  RESOURCE_ID_IMAGE_6, RESOURCE_ID_IMAGE_7, RESOURCE_ID_IMAGE_8,
  RESOURCE_ID_IMAGE_9, RESOURCE_ID_IMAGE_10
};

static GBitmap *images[TOTAL_IMAGE_SLOTS];
static BitmapLayer *image_layers[TOTAL_IMAGE_SLOTS];

#define EMPTY_SLOT -1
  
int colour = 0;       //text colour default is red
int bColour = 6;      //background colour default is black
GColor colourList[8]; //ordinary colours
GColor dColours[8];   //darker colours

// The state is either "empty" or the digit of the image currently in
// the slot--which was going to be used to assist with de-duplication
// but we're not doing that due to the one parent-per-layer
// restriction mentioned above.
static int image_slot_state[TOTAL_IMAGE_SLOTS] = {EMPTY_SLOT, EMPTY_SLOT, EMPTY_SLOT, EMPTY_SLOT};

static void load_digit_image_into_slot(int slot_number, int digit_value) {
  /*

     Loads the digit image from the application's resources and
     displays it on-screen in the correct location.

     Each slot is a quarter of the screen.

   */

  if ((slot_number < 0) || (slot_number >= TOTAL_IMAGE_SLOTS)) {
    return;
  }

  if ((digit_value < 0) || (digit_value > 10)) {
    return;
  }

  if (image_slot_state[slot_number] != EMPTY_SLOT) {
    return;
  }

  image_slot_state[slot_number] = digit_value;
  images[slot_number] = gbitmap_create_with_resource(IMAGE_RESOURCE_IDS[digit_value]);
  
  GRect frame = (GRect) {
    .origin = { (slot_number % 2) * 72, (slot_number / 2) * 84 },
    .size = gbitmap_get_bounds(images[slot_number]).size
  };
  
  BitmapLayer *bitmap_layer = bitmap_layer_create(frame);
  image_layers[slot_number] = bitmap_layer;
  bitmap_layer_set_bitmap(bitmap_layer, images[slot_number]);
  
  //Changes the default colours to the new colours.
  replace_gbitmap_color(GColorWhite, colourList[colour], images[slot_number], bitmap_layer);
  replace_gbitmap_color(GColorLightGray, dColours[colour], images[slot_number], bitmap_layer);
  replace_gbitmap_color(GColorDarkGray, dColours[colour], images[slot_number], bitmap_layer);
  replace_gbitmap_color(GColorBlack, colourList[bColour], images[slot_number], bitmap_layer);
  
  Layer *window_layer = window_get_root_layer(window);
  layer_add_child(window_layer, bitmap_layer_get_layer(bitmap_layer));
}

static void unload_digit_image_from_slot(int slot_number) {
  /*

     Removes the digit from the display and unloads the image resource
     to free up RAM.

     Can handle being called on an already empty slot.

   */

  if (image_slot_state[slot_number] != EMPTY_SLOT) {
    layer_remove_from_parent(bitmap_layer_get_layer(image_layers[slot_number]));
    bitmap_layer_destroy(image_layers[slot_number]);
    gbitmap_destroy(images[slot_number]);
    image_slot_state[slot_number] = EMPTY_SLOT;
  }

}

static void display_value(unsigned short value, unsigned short row_number, bool show_first_leading_zero) {
  /*

     Displays a numeric value between 0 and 99 on screen.

     Rows are ordered on screen as:

       Row 0
       Row 1

     Includes optional blanking of first leading zero,
     i.e. displays ' 0' rather than '00'.

   */
  value = value % 100; // Maximum of two digits per row.
  
  // Column order is: | Column 0 | Column 1 |
  // (We process the columns in reverse order because that makes
  // extracting the digits from the value easier.)
  
  //This block changes how the time is displayed so that the 10 character replaces the 0 in multiples of 10 and the leading 1 in the teens.
  int isATen = 0;
  for (int column_number = 1; column_number >= 0; column_number--) {
    int slot_number = (row_number * 2) + column_number;
    unload_digit_image_from_slot(slot_number);
    if (!((value == 0) && (column_number == 0)  )){
      //&&!show_first_leading_zero)) {
      unsigned short valueToShow = (column_number == 0 && value == 1) || (value % 10 ==0 && column_number ==1) ? 10 : value % 10;
      
      if((value == 0 && column_number ==1 &&row_number==0 )||(value==0&&row_number==1)){
        valueToShow=0; //fixes the bug where 00 was displayed as 10
      }
      
      if (column_number == 1 || value != 1 || !isATen) {
        load_digit_image_into_slot(slot_number, valueToShow);
      }
      
      if (valueToShow == 10) {
        isATen = 1;
      }
    }
		value = value / 10;
  }
}


static unsigned short get_display_hour(unsigned short hour) {
  if (clock_is_24h_style()) {
    return hour;
  }

  unsigned short display_hour = hour % 12;

  // Converts "0" to "12"
  return display_hour ? display_hour : 12;
}

static void display_time(struct tm *tick_time) {
  display_value(get_display_hour(tick_time->tm_hour), 0, false);
  display_value(tick_time->tm_min, 1, true);
}

static void handle_minute_tick(struct tm *tick_time, TimeUnits units_changed) {
  display_time(tick_time);
}

void netdownload_receive(DictionaryIterator *iter, void *context){
  Tuple *tuple = dict_read_first(iter);
  
  if(!tuple){
    APP_LOG(APP_LOG_LEVEL_ERROR, "Got a message with no first key! Size of message: %lu", (uint32_t)iter->end - (uint32_t)iter->dictionary);
    return;    
  }

  char * dataRecieved = tuple->value->cstring;
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Data Recieved: %s", dataRecieved);

  
  time_t now = time(NULL);
  struct tm *tick_time = localtime(&now);
  display_time(tick_time);

  //If both colours were recieved, change the data recieved to the colour value.
  if(strstr(dataRecieved, "INCORRECT")!=NULL){
    APP_LOG(APP_LOG_LEVEL_INFO, "Incorrect value was recieved.");
  }
  else{
    //converts %225%22%223%22 to 5
    char c[2];
    c[0] = *(dataRecieved + 3);
    c[1] = '\0';
    colour = atoi(c);

    //converts %225%22%223%22 to 3
    char b[2];
    b[0] = *(dataRecieved + 10);
    b[1] = '\0';
    bColour = atoi(b);
  }
}

void recieveMessage(){
  app_message_register_inbox_received(netdownload_receive);
  app_message_open(200,200);
}

void initColours() {
  colourList[0] = GColorRed;
  colourList[1] = GColorGreen;
  colourList[2] = GColorBlue;
  colourList[3] = GColorYellow;
  colourList[4] = GColorCyan;
  colourList[5] = GColorMagenta;
  colourList[6] = GColorBlack;
  colourList[7] = GColorWhite;
}

//these colours are darker and are used for the grey area around the white text.
void initDColours(){
  dColours[0]=GColorDarkCandyAppleRed;
  dColours[1]=GColorIslamicGreen;
  dColours[2]=GColorDukeBlue;
  dColours[3]=GColorLimerick;
  dColours[4]=GColorTiffanyBlue;
  dColours[5]=GColorJazzberryJam;
  dColours[6]=GColorBlack;
  dColours[7]=GColorLightGray;
}

static void init() {
  initColours();
  initDColours();
  recieveMessage();
  window = window_create();
  window_stack_push(window, true);
  window_set_background_color(window, GColorBlack);
  // Avoids a blank screen on watch start.

  tick_timer_service_subscribe(MINUTE_UNIT, handle_minute_tick);
  time_t now = time(NULL);
  struct tm *tick_time = localtime(&now);
  display_time(tick_time);
}

static void deinit() {
  for (int i = 0; i < TOTAL_IMAGE_SLOTS; i++) {
    unload_digit_image_from_slot(i);
  }
  window_destroy(window);
}

int main(void) {
  init();
  app_event_loop();
  deinit();
}